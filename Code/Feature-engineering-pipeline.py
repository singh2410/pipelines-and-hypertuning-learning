#!/usr/bin/env python
# coding: utf-8

# # Feature engineering pipeline and hyperparatuning
# #By- Aarush Kumar
# #Dated: October 21,2021

# In[1]:


get_ipython().system('pip install feature-engine')


# In[2]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
# for the model
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import roc_auc_score
from sklearn.pipeline import Pipeline
# for feature engineering
from feature_engine import imputation as mdi
from feature_engine import encoding as ce


# ## Load the data

# In[3]:


data = pd.read_csv("/home/aarush100616/Downloads/Projects/Pipeline-and-hyperparam-tuning/Data/train.csv")
data


# In[4]:


cols = [
    'Pclass', 'Sex', 'Age', 'SibSp', 'Parch', 'Fare', 'Cabin',
    'Embarked', 'Survived'
]
data = data[cols]
data.head()


# In[5]:


data['cabin_num'] = data['Cabin'].str.extract('(\d+)') # captures numerical part
data['cabin_num'] = data['cabin_num'].astype('float')
data['cabin_cat'] = data['Cabin'].str[0] # captures the first letter
data.drop(['Cabin'], axis=1, inplace=True)
data.head()


# In[6]:


# numerical: discrete
discrete = [
    var for var in data.columns if data[var].dtype != 'O' and var != 'Survived'
    and data[var].nunique() < 10
]
# numerical: continuous
continuous = [
    var for var in data.columns
    if data[var].dtype != 'O' and var != 'Survived' and var not in discrete
]
# categorical
categorical = [var for var in data.columns if data[var].dtype == 'O']
print('There are {} discrete variables'.format(len(discrete)))
print('There are {} continuous variables'.format(len(continuous)))
print('There are {} categorical variables'.format(len(categorical)))


# In[7]:


discrete


# In[8]:


continuous


# In[9]:


categorical


# In[10]:


# separate into training and testing set
X_train, X_test, y_train, y_test = train_test_split(
    data.drop('Survived', axis=1),  # predictors
    data['Survived'],  # target
    test_size=0.1,  # percentage of obs in test set
    random_state=0)  # seed to ensure reproducibility
X_train.shape, X_test.shape


# ## Set up the pipeline

# In[11]:


titanic_pipe = Pipeline([
    # missing data imputation - we replace na in numerical variables
    # with an arbitrary value. 
    ('imputer_num',
     mdi.ArbitraryNumberImputer(arbitrary_number=-1,
                                variables=['Age', 'Fare', 'cabin_num'])),
    # for categorical variables, we can either replace na with the string
    # missing or with the most frequent category
    ('imputer_cat',
     mdi.CategoricalImputer(variables=['Embarked', 'cabin_cat'])),
    # categorical encoding - we will group rare categories into 1
    ('encoder_rare_label', ce.RareLabelEncoder(
        tol=0.01,
        n_categories=2,
        variables=['Embarked', 'cabin_cat'],
    )),
    # we replace category names by numbers
    ('categorical_encoder', ce.OrdinalEncoder(
        encoding_method='ordered',
        variables=['cabin_cat', 'Sex', 'Embarked'],
    )),
    # Gradient Boosted machine
    ('gbm', GradientBoostingClassifier(random_state=0))
])


# ## Grid Search with Cross-validation

# In[13]:


param_grid = {
    # try different feature engineering parameters:
    # test different parameters to replace na with numbers
    'imputer_num__arbitrary_number': [-1, 99],
    # test imputation with frequent category or string missing
    'imputer_cat__imputation_method': ['missing','frequent'],
    # test different thresholds to group rare labels
    'encoder_rare_label__tol': [0.1, 0.2],
    # test 2 different encoding strategies
    'categorical_encoder__encoding_method': ['ordered', 'arbitrary'],   
    # try different gradient boosted tree model paramenters
    'gbm__max_depth': [None, 1, 3],
    'gbm__n_estimators': [10, 20, 50, 100, 200]
}


# In[14]:


grid_search = GridSearchCV(
    titanic_pipe, # the pipeline
    param_grid, # the hyperparameter space
    cv=3, # the cross-validation
    scoring='roc_auc', # the metric to optimize
)


# In[15]:


grid_search.fit(X_train, y_train)

# and we print the best score over the train set
print(("best roc-auc from grid search: %.3f"
       % grid_search.score(X_train, y_train)))


# In[16]:


print(("best linear regression from grid search: %.3f"
       % grid_search.score(X_test, y_test)))


# In[17]:


grid_search.best_estimator_


# In[18]:


grid_search.best_params_


# In[19]:


results = pd.DataFrame(grid_search.cv_results_)
print(results.shape)
results.head()


# In[20]:


results.sort_values(by='mean_test_score', ascending=False, inplace=True)
results.reset_index(drop=True, inplace=True)
# plot model performance and the generalization error
results['mean_test_score'].plot(yerr=[results['std_test_score'], results['std_test_score']], subplots=True)
plt.ylabel('Mean test score - ROC-AUC')
plt.xlabel('Hyperparameter combinations')


# In[21]:


importance = pd.Series(grid_search.best_estimator_['gbm'].feature_importances_)
importance.index = data.drop('Survived', axis=1).columns
importance.sort_values(inplace=True, ascending=False)
importance.plot.bar(figsize=(12,6))

